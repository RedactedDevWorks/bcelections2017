INSERT INTO webapp_candidate (name, slug, represents_id, bio) VALUES ('Andrew J. Weaver', 'andrew-weaver', 1, 'Dr. Andrew J. Weaver is a Canadian scientist and politician representing the riding of Oak Bay-Gordon Head in the British Columbia Legislative Assembly. Weaver is the leader of the Green Party of British Columbia.');
INSERT INTO webapp_candidate (name, slug, represents_id, bio) VALUES ('Christy Clark', 'christy-clark', 2, 'Christina Joan "Christy" Clark, MLA is a Canadian politician who currently serves as the 35th premier of British Columbia, Canada.');
INSERT INTO webapp_candidate (name, slug, represents_id, bio) VALUES ('John Horgan', 'john-horgan', 3, 'John Horgan is the leader of the British Columbia New Democratic Party and MLA for the constituency of Juan de Fuca in the Canadian province of British Columbia. He was born and raised in Victoria, British Columbia.');
INSERT INTO webapp_candidate (name, slug, represents_id, bio) VALUES ('Clayton Welwood', 'clayton-welwood', 4, 'N/A');
INSERT INTO webapp_candidate (name, slug, represents_id, bio) VALUES ('James Filippelli', 'james-filippelli', 6, 'N/A');
INSERT INTO webapp_candidate (name, slug, represents_id, bio) VALUES ('Timothy Gidora', 'timothy-gidora', 7, 'N/A');
INSERT INTO webapp_candidate (name, slug, represents_id, bio) VALUES ('Rod Taylor', 'rod-taylor', 8, 'N/A');
INSERT INTO webapp_candidate (name, slug, represents_id, bio) VALUES ('Robin Richardson', 'robin-richardson', 9, 'Robin Mark Richardson (born 26 June 1942) is a former Canadian politician who was a Progressive Conservative member of the Canadian House of Commons. He represented the Toronto, Ontario riding of Beaches from 1979 to 1980.

He represented Ontario''s Beaches electoral district which he won in the 1979 federal election.[3] After serving his only term, the 31st Canadian Parliament, he was defeated in the 1980 federal election by Neil Young of the New Democratic Party.

In September 2000, he unsuccessfully challenged incumbent Esquimalt—Juan de Fuca Member of Parliament Keith Martin for the Canadian Alliance nomination in that riding. Richardson was particularly critical of Martin''s pro-choice position on abortion, while Martin had finished in fourth place during the Canadian Alliance leadership campaign earlier that year.[2] Richardson managed Stockwell Day''s successful leadership campaign within Esquimalt—Juan de Fuca.

In June 2016 he started the Vancouver Island Party and serves as leader. The party seeks to make Vancouver Island Canada''s 11th province.');
INSERT INTO webapp_candidate (name, slug, represents_id, bio) VALUES ('Troy Gibbons', 'troy-gibbons', 12, '');
INSERT INTO webapp_candidate (name, slug, represents_id, bio) VALUES ('Erik Deutscher', 'erik-deutscher', 14, '');
INSERT INTO webapp_candidate (name, slug, represents_id, bio) VALUES ('Salvatore Vetro', 'salvatore-vetro', 15, '');
INSERT INTO webapp_candidate (name, slug, represents_id, bio) VALUES ('Phillip Ryan', 'phillip-ryan', 16, '');
INSERT INTO webapp_candidate (name, slug, represents_id, bio) VALUES ('Mervyn Ritchie', 'mervyn-ritchie', 17, '');
INSERT INTO webapp_candidate (name, slug, represents_id, bio) VALUES ('Wei Chen', 'wei-chen', 18, '');