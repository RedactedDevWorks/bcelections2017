from django.db import models


class Party(models.Model):
    name = models.CharField(max_length=100)
    slug = models.SlugField(max_length=100)
    bio = models.TextField(blank=True, null=True)
    leader = models.ForeignKey('Candidate', default=None, blank=True, null=True)

    def __str__(self):
        return self.name


class Candidate(models.Model):
    name = models.CharField(max_length=100)
    slug = models.SlugField(max_length=100)
    bio = models.TextField(blank=True, null=True)
    represents = models.ForeignKey('Party')

    def __str__(self):
        return self.name


class PlatformCategory(models.Model):
    name = models.CharField(max_length=100)
    slug = models.SlugField(max_length=100)
    bio = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.name


class Platform(models.Model):
    name = models.CharField(max_length=100)
    slug = models.SlugField(max_length=100)
    bio = models.TextField(blank=True, null=True)
    category = models.ForeignKey(PlatformCategory)

    def __str__(self):
        return self.name


class Promise(models.Model):
    party = models.ForeignKey(Party)
    platform = models.ForeignKey(Platform)
    position = models.SmallIntegerField()
    notes = models.TextField(blank=True, null=True)

    def __str__(self):
        return "{} is {} on {}".format(self.party, self.position, self.platform)

