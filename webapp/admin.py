from django.contrib import admin

# Register your models here.
from .models import *


class PartyAdmin(admin.ModelAdmin):
    empty_value_display = 'vacant'
    list_display = ('name', 'leader_name',)
    prepopulated_fields = {"slug": ("name",)}

    def leader_name(self, obj):
        return obj.leader.name if obj.leader else "vacant"


class CandidateAdmin(admin.ModelAdmin):
    list_display = ('name','party_name',)
    prepopulated_fields = {"slug": ("name",)}

    def party_name(self, obj):
        return obj.represents.name


class PlatformCategoryAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("name",)}


class PlatformAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("name",)}


admin.site.register(Party, PartyAdmin)
admin.site.register(Candidate, CandidateAdmin)
admin.site.register(PlatformCategory, PlatformCategoryAdmin)
admin.site.register(Platform, PlatformAdmin)
admin.site.register(Promise)
